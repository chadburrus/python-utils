from subprocess import call

class Runner(object):
	"""
		A class to simplify running external programs.  You'll want to extend this
		class and implement __init__, handle_ret_code, and kickoff, at minimum, for
		each general command you want to use.
	"""

	def _run(self, command = [], stdin = None, stdout = None, shell = False):
		"""
			Don't call directly!  Use kickoff.
			Wrapper around subprocess.call. See
			http://docs.python.org/library/subprocess.html for full details.
		"""
		ret_code = call(command, stdin=stdin, stdout=stdout, shell=shell)
		if ret_code:
			self.handle_ret_code(ret_code, command, stdin, stdout, shell)

	def handle_ret_code(self, ret_code, command, stdin, stdout, shell):
		"""
			Exception handler in case something breaks running an external command.
			Should be overridden in child classes (I.e., in Java this would be
			"abstract").  Parameters are as in self.run, with the exception of the
			first parameter, which is the actual return code from the command
			execution.
		"""

		raise RuntimeError("Command Failed!!!")

	def super_me(self):
		""" A shortcut method to call super on me. """
		return super(type(self), self)

	def kickoff(self, command = [], stdin = None, stdout = None, shell = False):
		"""
			Actually kick off a run of the command.  Use this for interface
			consistency.
		"""
		self._run(command, stdin, stdout, shell)
