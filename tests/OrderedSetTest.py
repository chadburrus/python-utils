import init
import os
from OrderedSet import OrderedSet
from nose.tools import eq_

def test_works():
	set = (1, 3, 5, 1, 5, 2, 6)
	ordered_set = (1, 3, 5, 2, 6)

	o_set = OrderedSet(set)
	o_ordered_set = OrderedSet(ordered_set)

	eq_(o_set, o_ordered_set)

def test_add():
	set = (1, 3, 5, 2, 6)

	o_set = OrderedSet(set)
	o_set.add(9)

	new_set = OrderedSet(list(set) + [9])
	eq_(o_set, new_set)
