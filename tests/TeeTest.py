import init
import os
from Tee import Tee

def check_file(file_name, text):
	infile = open (file_name)
	contents = "\n".join(infile.readlines())
	assert text.strip() == contents.strip()

def test_without_mode():
	# init Tee
	file_name = 'modeless.tee'
	tee = Tee(file_name)
	
	# print some junk
	junk = """ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus velit a sem pretium id tristique eros consequat. In hac habitasse platea dictumst. Vestibulum id sapien at massa imperdiet molestie quis eget odio. Quisque quis pulvinar urna. Vivamus viverra pharetra iaculis. Aenean hendrerit aliquam imperdiet. Duis sit amet diam sit amet quam scelerisque consequat quis eu mauris. Sed a turpis nec ligula consequat iaculis. Aenean odio risus, adipiscing nec vestibulum non, tempus at tortor. Ut lacinia risus blandit elit auctor luctus. Pellentesque pharetra sodales malesuada. Integer quis nulla eget sapien lobortis vestibulum. Ut vel metus a augue tempor porttitor sit amet in mi. Quisque venenatis ligula ac odio gravida varius."""
	print junk

	# cleanup
	tee.flush()
	del tee

	# check things out
	check_file(file_name, junk)
	os.remove(file_name)

def test_with_mode():
	# init Tee
	file_name = 'mode.tee'
	tee = Tee(file_name)
	
	# print some junk
	junk = """ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus velit a sem pretium id tristique eros consequat. In hac habitasse platea dictumst. Vestibulum id sapien at massa imperdiet molestie quis eget odio. Quisque quis pulvinar urna. Vivamus viverra pharetra iaculis. Aenean hendrerit aliquam imperdiet. Duis sit amet diam sit amet quam scelerisque consequat quis eu mauris. Sed a turpis nec ligula consequat iaculis. Aenean odio risus, adipiscing nec vestibulum non, tempus at tortor. Ut lacinia risus blandit elit auctor luctus. Pellentesque pharetra sodales malesuada. Integer quis nulla eget sapien lobortis vestibulum. Ut vel metus a augue tempor porttitor sit amet in mi. Quisque venenatis ligula ac odio gravida varius."""
	print junk

	# cleanup
	tee.flush()
	del tee

	# check things out
	check_file(file_name, junk)
	os.remove(file_name)

